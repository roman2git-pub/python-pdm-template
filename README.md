# Python Project Template

A modern python project template using PDM (Python Development Master) and pyenv

| Field            | Details                         |
| ---------------- | ------------------------------- |
| Topic            | Python Development Master (PDM) |
| Operating System | Fedora 35 Workstation           |
| Source           | https://pdm.fming.dev/latest/   |
| Python           | >= 3.11                         |

**pyenv — Simple Python version management ([Link](https://github.com/pyenv/pyenv))**

Allows you to manage your own version of python especially useful when dealing with older/newer versions of Python not supported by the base system (also supports conda, pypy etc.). It works by downloading tarball of the package, compiling it and setting the environment variables

**PDM - Python Development Master ([Link](https://pdm.fming.dev/))**

PDM is a modern Python package manager with PEP 582 support i.e. it installs and manages packages in a similar way to npm that doesn't need to create a virtualenv at all. In addition, rocks the latest technologies in python such as the PEP 517 build backend, PEP 621 project metadata, a plugin system and even a optable centralized installation cache

## Setting up the Development Environment

Installing the prerequisite system packages (Update: This is an optional step, not required anymore if you are using dev-containers)

```bash
sudo dnf update # updates your system packages
sudo dnf groupinstall "Development Tools" # install the common development tools
sudo dnf install libffi-devel zlib-devel bzip2-devel readline-devel sqlite-devel wget curl llvm ncurses-devel openssl-devel lzma-sdk-devel libyaml-devel redhat-rpm-config # install the additional tools required for compilation
```

Installing pyenv (using the [pyenv installer](https://github.com/pyenv/pyenv-installer)) (Update: This is an optional step, not required anymore if you are using dev-containers)

```bash
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
# or
curl https://pyenv.run | bash
```

Note: Uninstalling pyenv

```bash
rm -fr ~/.pyenv # delete the folder

# remove from ~/.bashrc
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv virtualenv-init -)"

# apply the changes
source ~/.bashrc
```

Installing PDM (Update: This is an optional step, not required anymore if you are using dev-containers)

 ```bash
 pip install --upgrade pip # upgrade pip (optional)
 pip install pdm # run later after project setup is done # recommended
 # or
 curl -sSL https://raw.githubusercontent.com/pdm-project/pdm/main/install-pdm.py | python3 - # not-recommended at this time
 ```

## Creating your Project

Step 1: Create your project directory

Step 2: Setup python version for your project (Update: This is an optional step, not required anymore due to dev-containers)

```bash
touch .python-version # 3.9.10 is used for this project
pyenv install # will pickup version from the above file and install
pip install pdm # will install pdm for the above python version globally for the user
```

Step 3: Initialise your project

```bash
pdm init # follow the prompts to add the required project details
# creates pyproject.toml as shown below (and .pdm.toml)
```

```bash
[project]
name = "Python PDM Template"
version = "0.0.1"
description = "A modern python project"
authors = [
    {name = "Chetan Munigangappa", email = "chetangangappa94@gmail.com"},
]

# modified
dependencies = [
	"requests>=2.27.1",
]

requires-python = ">=3.9"
license = {text = "MIT"} # can be removed

# can be removed
[project.urls]
Homepage = ""

# modified (only required if using a optional dependencies)
[project.optional-dependencies]
extra1 = ["flask"]
extra2 = ["django"]

# modified (only required if using a development dependencies)
[tool.pdm.dev-dependencies]
test = ["pytest"]

[build-system]
requires = ["pdm-pep517"]
build-backend = "pdm.pep517.api"

# modified (only required if using a custom source)
[[tool.pdm.source]]
url = "https://pypi.org/simple"
name = "pypi"
verify_ssl = true

# modified (only required if scripts are involved)
[tool.pdm.scripts]
shell_example = {shell = "pdm install && python ./src/main/Main.py"} # shell script
arbitrary = "python ./src/main/Main.py" # arbitrary command
start.env = {FOO = "bar", FLASK_ENV = "development"} # setting environment variables
```

Step 4: Write some python code

```python
# ./src/main/__init__.py # empty init file
# ./src/main/Main.py # source file with below contents

print("Hello World")
```

Step 5: Run the project

```bash
pdm run shell_example
```

Additional Info: 

Setting up the .gitignore file

```bash
/__pypackages__/ # project directory where the python packages are stored (can be skipped to reduce repo size)
.pdm.toml # project pdm configuration file - recommended to be skipped in favor of global defaults
/dist/ # project build aritifacts
```

Useful PDM commands

| Command                                                      | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `pdm install`                                                | Install production and development dependencies (no optional) |
| `pdm install --prod`                                         | Install production dependencies only                         |
| `pdm install -G extra1`                                      | Install optional dependency with label extra1 in addition to production and development dependencies |
| `pdm build`                                                  | Builds distribution artifacts (.whl)                         |
| `pdm export -o requirements.txt`, `pdm export -f setuppy -o setup.py` | Export locked packages to alternative formats (requirements.txt, setup.py are supported) |
| `pdm run <script>`                                           | Run arbitrary or custom scripts from pyproject using the run command |

## Additional References

- [PDM - Official Website](https://pdm.fming.dev/latest/)
